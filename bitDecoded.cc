//////////////////////////////////////////////////////////////////////////////
// Copyright 2017 Ying-Chun Lai                                             //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
//////////////////////////////////////////////////////////////////////////////

#include "stdio.h"
#include "stdlib.h"
#include <string>
#include <sstream>
#include <iomanip>
#include <set>
#include <algorithm>

using namespace std;

const int MAX_NR_OF_BITS = 32;
set<int> getFields(int argc, char **argv)
{
   set<int> v {0,4,8,12,16,20,24,28};
   if (argc > 2)
   {
      v.clear();
      for (int i = 2; i< argc; ++i)
      {
         v.insert(strtoul(argv[i], NULL, 0));
      }
   }
   v.insert(MAX_NR_OF_BITS);

   return v;
}

unsigned int getValueInField(int upperBit, int nrOfBits, int val)
{
   unsigned int shift = upperBit - nrOfBits +1;
   unsigned int mask = ((1 << nrOfBits) -1) << shift;

   return (val & mask) >> shift;
}

int main (int argc, char **argv)
{
   if (argc == 1)
   {
      printf("Missing variable to be parsed.\n");
      printf("Usage: bitDecoded <value> [bit0] [bit1] [bit2] ..\n");
      return 0;
   }

   // TODO: add the "help, -h" support
   // get the value to decode
   string valStr(argv[1]);
   int base = 0;

   if (valStr.find("0b") != string::npos)
   {
      base = 2;
      valStr.erase(0, 2);
   }

   // TODO:check the invalid argument
   unsigned int val = strtoul(valStr.c_str(), NULL, base);

   printf("Value = 0x%x, %u (%u)\n", val, val, val);

   // print the formated strings.
   string bit, base2, base10;

   const unsigned int whitespacePerBit = 3;
   unsigned int nrOfWhitespace = whitespacePerBit; // whitespacePerBit * nrOfBits

   set<int> fields = getFields(argc, argv);

   for (unsigned int i = 0; i < MAX_NR_OF_BITS ; ++i)
   {
      ostringstream s, s2, s10;
      s << setw(whitespacePerBit) << i;
      bit.insert(0, s.str());

      if (fields.find(i+1) != fields.end())
      {
         s2 << "|" << setw(whitespacePerBit-1)<< ((val & (1 << i)) >> i);

         unsigned int valMasked = getValueInField(i, nrOfWhitespace/whitespacePerBit, val);
         s10 << "|" << setw(nrOfWhitespace-1) << valMasked;
         nrOfWhitespace = whitespacePerBit; // reset the space whitespacePerBit.
      }
      else
      {
         nrOfWhitespace += whitespacePerBit;
         s2 << setw(whitespacePerBit)<< ((val & (1 << i)) >> i);
      }

      base2.insert(0, s2.str());
      base10.insert(0, s10.str());
   }

   bit.insert(0, "bit\t");
   base2.insert(0, "base2\t");
   base10.insert(0, "base10\t");

   printf("%s\n%s\n%s\n", bit.c_str(), base2.c_str(), base10.c_str());

   return 0;
}
